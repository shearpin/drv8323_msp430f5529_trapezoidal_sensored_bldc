#include "msp430.h"
#include "dataTypeDefinition.h"
#include "mdbuserial.h"
#include "mdbuserial_protocol.h"

// Serial Command Functions
#define CMD_READ_REGISTER   0x01
#define CMD_WRITE_REGISTER  0x02
#define CMD_GET_GPIO        0x03
#define CMD_SET_GPIO        0x04
#define CMD_STOP_MOTOR      0x20
#define CMD_START_MOTOR     0x21
#define CMD_GET_MTR_PARAM   0x22
#define CMD_SET_MTR_PARAM   0x23
#define CMD_GET_CTRL_TYPE   0x24
#define CMD_SET_CTRL_TYPE   0x25
#define CMD_GET_CTRL_REF    0x26
#define CMD_SET_CTRL_REF    0x27

void cmd_readRegister(uint8_t* data, size_t length);
void cmd_writeRegister(uint8_t* data, size_t length);
void cmd_getGPIO(uint8_t* data, size_t length);
void cmd_setGPIO(uint8_t* data, size_t length);
void cmd_stopMotor(uint8_t* data, size_t length);
void cmd_startMotor(uint8_t* data, size_t length);
void cmd_getMtrParam(uint8_t* data, size_t length);
void cmd_setMtrParam(uint8_t* data, size_t length);
void cmd_getCtrlType(uint8_t* data, size_t length);
void cmd_setCtrlType(uint8_t* data, size_t length);
void cmd_getCtrlRef(uint8_t* data, size_t length);
void cmd_setCtrlRef(uint8_t* data, size_t length);

extern const _mdbuserial_callbacktable_t mdbuserial_callbacktable[];
