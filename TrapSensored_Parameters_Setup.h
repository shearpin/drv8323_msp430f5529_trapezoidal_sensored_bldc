/**************************************************************************
 * @file        TrapSensored_Parameters_Setup.h
 * @author      MDBU Software Team
 * @brief       Parameters to be set up by the User.
 * @note        Copyright (c) 2016 Texas Instruments Incorporated.
 *              All rights reserved.
 ******************************************************************************/

/* System parameter setup */
#define ALGO_ID	(0)       						/* 1 Indicates Sensorless, 0 indicates Sensored algorithms */
#define SPEED_INPUT_SAMP_INTERVAL (127)         /* The number of PWM cycles after which change in Speed input through ADC is Sampled */
#define PWM_FACTOR (0)	          				/* 12 bit ADC result registers will be scaled to PWM period which is 10 bit  */
#define PWM_PERIOD (1000)						/* PWM Period time , With a 25Mhz clock , PWM will be generated at 25Khz*/
#define READ_VCC_PERIOD (100)					/* TIME INTERVAL AFTER WHICH VCC IS MONITORED (100ms) */
#define TIME_COUNT_1MS (25000)            		/* 1 ms timer count */
#define MIN_DUTY_CYCLE	(15)             	    /* Minimal duty cycle applied to motor */
#define MAX_DUTY_CYCLE	(1000)              	/* Maximal duty cycle applied to motor */
#define RAMP_RATE (5)							/* Ramp rate for acceleration and deceleration of the motor speed*/
#define RAMP_RATE_DELAY (20)		        	/* How many PWM periods are between a change of the speed */
/* Fault handling setup */						/* ADC Max ref voltage is 3.3v ,  VCC is scaled by 0.074 internally (5/62 Ohms bridge) so that VCC ref input to ADC never cross 3.3v.*/
#define UNDER_VOLTAGE_LIMIT (569)			    /* 8V Under Voltage set for below 10.0V - (10*4096)/57.5 = 712  */
#define OVER_VOLTAGE_LIMIT	(1780)     			/* Over Voltage set for above 20.0V  - (20*4096)/57.5 = 1424 */
												/*As BEMF for above 23 volts may give ADC ref volts exceeds nominal value 2.4V , MAX VCC is limited to 23.0v*/
#define MIN_STALLDETECT_DUTY		(125)		/* Minimum Duty Cycle above which stall will be detected */
#define STALLDETECT_REV_THRESHOLD	(1)			/* Number of revolutions below which stall fault will be flagged */
#define STALLDETECT_TIMER_THRESHOLD (500)		/* Time in milli seconds above which if motor doesnt spin min revolutions specified above(STALLDETECT_REV_THRESHOLD) a stall fault is triggered */
#define AUTO_FAULT_RECOVERY_TIME (3000)      	/* Time in milli seconds after which system reinitialises itself if fault gets cleared */
#define STALL_DETECTOIN_FLAG (1)            	/* Enable motor stall fault detection*/
#define VCC_MONITOR_FLAG (1)                	/* Enable VCC Supply Voltage monitoring for fault detection */
#define MOTOR_PHASE_CURRENT_LIMIT (2606)        /* 15 amps @ 20 V/V gain w/ 7mOhm sense Defines the max allowed motor phase current in digital counts. Motor phase current is monitored every electrical cycle , and when ever current limit is reached an OC fault is Triggered */

/* DRV8323 SPI REGISTER SETTINGS*/
/* SPI_REG_02 : DRIVER CONTROL */
#define RSVD        (0x00)         /*  */
#define DIS_CPUV    (0x00)         /*  */
#define DIS_GDF     (0x00)         /*  */
#define OTW_REP     (0x00)         /*  */
#define PWM_MODE    (0x00)         /*  */
#define PWM_COM     (0x00)         /*  */
#define PWM_DIR     (0x00)         /*  */
#define COAST_BIT   (0x00)         /*  */
#define BRAKE_BIT   (0x00)         /*  */
#define CLR_FLT     (0x00)         /*  */

/* SPI_REG_03 : GATE DRIVE HS */
#define LOCK_BIT    (0x03)         /*  */
#define IDRIVEP_HS  (0x0F)         /*  */
#define IDRIVEN_HS  (0x0F)         /*  */

/* SPI_REG_04 : GATE DRIVE LS */
#define CBC         (0x01)         /*  */
#define TDRIVE      (0x03)         /*  */
#define IDRIVEP_LS  (0x0F)         /*  */
#define IDRIVEN_LS  (0x0F)         /*  */

/* SPI_REG_05 : OCP CONTROL */
#define TRETRY      (0x00)         /*  */
#define DEAD_TIME   (0x00)         /*  */
#define OCP_MODE    (0x01)         /*  */
#define OCP_DEG     (0x01)         /*  */
#define VDS_LVL     (0x09)         /*  */

/* SPI_REG_06 : CSA CONTROL */
#define CSA_FET     (0x00)         /*  */
#define VREF_DIV    (0x01)         /*  */
#define LS_REF      (0x00)         /*  */
#define CSA_GAIN    (0x01)         /*  */
#define DIS_SEN     (0x00)         /*  */
#define CSA_CAL_A   (0x00)         /*  */
#define CSA_CAL_B   (0x00)         /*  */
#define CSA_CAL_C   (0x00)         /*  */
#define SEN_LVL     (0x03)         /*  */
