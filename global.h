/**************************************************************************
 * @file        global.c
 * @author      MDBU Software Team
 * @brief       This file defines all the functions used by application during runtime
 * @note        Copyright (c) 2016 Texas Instruments Incorporated.
 *              All rights reserved.
 ******************************************************************************/

#ifndef global_H_
#define global_H_

#include "Init.h"
#include "dataTypeDefinition.h"
#include "msp430.h"
#include "SPI_API.h"
#include "TrapSensored_Parameters_Setup.h"
#include "mdbuserial.h"
#include "usb.h"

#define EN_DRV	    BIT6    // P1.6
#define nFAULT	    BIT7    // P2.7

/* Device register width in number of bytes */
#define DRV832X_REGISTER_WIDTH (2)

/* Device ID, Currently hardcoded to 'DRV8323S - Sensored', later it will be derived at runtime */
#define DRV832X_DEVICE_ID 	   (3)

void EnableGateDrivers();
void DisableGateDrivers();
void UpdateNextCommutation(void);
void PWM_SetCommutation(uint8_t hallState);
void ReadPotiSpeed(void);
void ReadVCC(void);
void ReadCurrentShunt(void);
void ReadSPDFDBK(void);

void drv832x_regRestoreFromCache();
void drv832x_setGPIO(unsigned char gpioPort, unsigned char gpioNum, unsigned char gpioVal);
unsigned char drv832x_getGPIO(unsigned char gpioPort, unsigned char gpioNum);
void drv832x_StartMotor();
void drv832x_StopMotor();
unsigned int drv832x_registerRead(unsigned char address);
void drv832x_registerWrite(unsigned char address, unsigned int value);
unsigned long drv832x_getMtrParam(unsigned char num);
void drv832x_setMtrParam(unsigned char num, unsigned long value);
void drv832x_setCtrlType(unsigned char value);
unsigned char drv832x_getCtrlType(void);

typedef enum
{
    HOST_EN = 0,
    HOST_IDLE = 1,
    HOST_ACTIVE = 2
} HOSTCONTROL_STATUS;

typedef struct HOST_CONTROLLER_Obj
{
	uint8_t EnabledGateDrivers;		/* 0 = Disabled, 1 = Enabled */
	uint8_t StartStopMotor;			/* 0 = Start, 1 = Stop */
} HOST_CONTROLLER_Obj;

typedef enum
{
    SYSTEM_INIT = 0,
    SYSTEM_IDLE = 1,
    MOTOR_IDLE = 2,
    MOTOR_RAMP_UP = 3,
    MOTOR_RUN = 4,
    MOTOR_RAMP_DOWN = 5,
    MOTOR_STOP = 6,
    FAULT = 7,
    MOTOR_DIRECTION = 8
}STATE_MACHINE;

typedef enum
{
    NOFAULT = 0,
	VOLTAGE = 1,
	OVERCURRENT = 2,
	OVERTEMPERATURE = 3,
	MOTOR_STALL = 4,
	HALLSENSORINVALID = 5,
	GATE_DRIVER = 6,
    UNKNOWN = 7
} FAULTS;

typedef struct APPLICATION_STATUS
{
    STATE_MACHINE currentstate;
    STATE_MACHINE previousstate;
    FAULTS fault;
} APPLICATION_STATUS;

typedef struct SENSORED_TRAP_Obj
{
    /*Acceleration variables*/
    /*Initialize a counter to compare with Acceleration Divider.*/
    uint16_t Accelerationcounter;
    /*How many PWM cycles before increasing duty cycle. */
    uint16_t Accelerationdivider;
    uint16_t MINDutyCycle;              /* Low speed setting depending on application*/
    uint16_t MAXDutyCycle;              /* High speed setting depending on application*/
    /*This keeps track of how many rotations (electrical) of the motor is completed since the last startup.*/
    uint16_t RotationCount;
    /*System Variables */
    uint8_t currentHallstate;
    uint8_t ADCReadState;      /* specifies in which state ADC read has been initaiated */
    uint8_t Direction;
    uint8_t Direction_flag; /* flag to monitor if direction change is required */
    BOOL    Direction_pin;      // direction indicated by input pin

    /*Duty cycle that should be reached for the selected potentiometer command setting.*/
    uint16_t TargetDutyCycle;
    /*Duty cycle that is going out on the PWM at this moment.*/
    uint16_t CurrentDutyCycle;
    uint16_t RampRate;                          /*How fast increasing and decreasing duty cycle*/
    /* Fault handling variables */
    uint16_t StallDetectCounter;         /*This is a counter used in Timer4 to check for the motor stall.*/
    uint16_t RestartDelay;              /*This is the variable that is used to count the time from when a fault is cleared*/
    uint16_t VCCvoltage;            /*How much voltage applied for EVM*/
    uint16_t MotorPhaseCurrent;        /* How much current flowing through motor phase */
    float_t MotorPhaseCurrentLimit;     /* Defines Maximum allowed Motor phase current  */
    uint16_t underVoltageLimit;            /*Configurable Under Voltage Limit from HOST*/
    uint16_t overVoltageLimit;            /*Configurable Over Voltage Limit from HOST*/
    uint16_t minStallDetectDuty; 		 /* Minimum Duty Cycle above which stall will be detected */
    uint16_t stallDetectRevThreshold; 	/* Number of revolutions below which stall fault will be flagged */
    uint16_t stallDetectTimerThreshold; /* Time in milli seconds above which if motor doesnt spin min revolutions specified above(stallDetectRevThreshold) a stall fault is triggered */
    uint16_t autoFaultRecoveryTime; 	/* Time in milli seconds after which system reinitialises itself if fault gets cleared */
    uint16_t readVccCounter;			/* Counter to read VCC every READ_VCC_PERIOD interval */
    BOOL TimerOverflowFlag;  			/* Interrupt counter to ensure the speed measurement with 16bit resolution*/
    uint16_t SPDFdbk;           		/* This variable holds the timer counts with 16bit resolution */
    uint16_t PWM_Mode;			  		/* This Variable holds the Ctrl type : 0 for 6PWM mode , 1 for 1 PWM mode*/
    uint16_t PWMPeriod;          		/* This Variable holds the Motor parameter to set the PWM switching frequency*/
    uint16_t DeviceID;         			/* This variable holds the Motor parameter (2) Device ID to set the appropriate motor control page for a connected device */
} SENSORED_TRAP_Obj;

typedef struct FLT_STAT_REG0_Obj
{
    uint8_t REG0_FAULT;      // bit 10
    uint8_t REG0_VDS_OCP;    // bit 9
    uint8_t REG0_GDF;        // bit 8
    uint8_t REG0_UVLO;       // bit 7
    uint8_t REG0_OTSD;       // bit 6
    uint8_t REG0_VDS_HA;     // bit 5
    uint8_t REG0_VDS_LA;     // bit 4
    uint8_t REG0_VDS_HB;     // bit 3
    uint8_t REG0_VDS_LB;     // bit 2
    uint8_t REG0_VDS_HC;     // bit 1
    uint8_t REG0_VDS_LC;     // bit 0
} FLT_STAT_REG0_Obj;

typedef struct VGS_STAT_REG1_Obj
{
    uint8_t REG1_SA_OC;      // bit 10
    uint8_t REG1_SB_OC;      // bit 9
    uint8_t REG1_SC_OC;      // bit 8
    uint8_t REG1_OTW;        // bit 7
    uint8_t REG1_CPUV;       // bit 6
    uint8_t REG1_VGS_HA;     // bit 5
    uint8_t REG1_VGS_LA;     // bit 4
    uint8_t REG1_VGS_HB;     // bit 3
    uint8_t REG1_VGS_LB;     // bit 2
    uint8_t REG1_VGS_HC;     // bit 1
    uint8_t REG1_VGS_LC;     // bit 0
} VGS_STAT_REG1_Obj;

typedef struct DRV_CTRL_REG2_Obj
{
    uint8_t REG2_RSVD;      // bit 10
    uint8_t REG2_DIS_CPUV;  // bit 9
    uint8_t REG2_DIS_GDF;   // bit 8
    uint8_t REG2_OTW_REP;   // bit 7
    uint8_t REG2_PWM_MODE;  // bit 6:5
    uint8_t REG2_PWM_COM;   // bit 4
    uint8_t REG2_PWM_DIR;   // bit 3
    uint8_t REG2_COAST;     // bit 2
    uint8_t REG2_BRAKE;     // bit 1
    uint8_t REG2_CLR_FLT;   // bit 0
} DRV_CTRL_REG2_Obj;

typedef struct GATE_DRV_HS_REG3_Obj
{
    uint8_t REG3_LOCK;          // bit 10:8
    uint8_t REG3_IDRIVEP_HS;    // bit 7:4
    uint8_t REG3_IDRIVEN_HS;    // bit 3:0
} GATE_DRV_HS_REG3_Obj;

typedef struct GATE_DRV_LS_REG4_Obj
{
    uint8_t REG4_CBC;           // bit 10
    uint8_t REG4_TDRIVE;        // bit 9:8
    uint8_t REG4_IDRIVEP_LS;    // bit 7:4
    uint8_t REG4_IDRIVEN_LS;    // bit 3:0
} GATE_DRV_LS_REG4_Obj;

typedef struct OCP_CTRL_REG5_Obj
{
    uint8_t REG5_TRETRY;        // bit 10
    uint8_t REG5_DEAD_TIME;     // bit 9:8
    uint8_t REG5_OCP_MODE;      // bit 7:6
    uint8_t REG5_OCP_DEG;       // bit 5:4
    uint8_t REG5_VDS_LVL;     // bit 3:0
} OCP_CTRL_REG5_Obj;

typedef struct CSA_CTRL_REG6_Obj
{
    uint8_t REG6_CSA_FET;       // bit 10
    uint8_t REG6_VREF_DIV;      // bit 9
    uint8_t REG6_LS_REF;        // bit 8
    uint8_t REG6_CSA_GAIN;      // bit 7:6
    uint8_t REG6_DIS_SEN;       // bit 5
    uint8_t REG6_CSA_CAL_A;     // bit 4
    uint8_t REG6_CSA_CAL_B;     // bit 3
    uint8_t REG6_CSA_CAL_C;     // bit 2
    uint8_t REG6_SEN_LVL;       // bit 1:0
} CSA_CTRL_REG6_Obj;

typedef struct REG_MAP_Obj
{
    uint16_t Fault_Status_Reg0;
    uint16_t VGS_Status_Reg1;
    uint16_t Driver_Control_Reg2;
    uint16_t Gate_Drive_HS_Reg3;
    uint16_t Gate_Drive_LS_Reg4;
    uint16_t OCP_Control_Reg5;
    uint16_t CSA_Control_Reg6;
} REG_MAP_Obj;

/////////////////////////
#endif /*global_H_*/
