/*************************************************************************
 * @file   DRV8323_MSP430F5529_trap_sensored_main.c
 * @author MDBU Software Team
 * @brief  Trapezoidal control of BLDC based on Hall-sensor input
 * @note   Copyright (c) 2016 Texas Instruments Incorporated.
 *         All rights reserved.
 ****************************************************************************/

#include "global.h"
#include "mdbu_global.h"

// Controller
SENSORED_TRAP_Obj sensoredTrapController;
APPLICATION_STATUS applicationStatus;

// Host Controller
HOSTCONTROL_STATUS HostControl_Status;
HOST_CONTROLLER_Obj HostController;
extern MDBUSERIAL_RXSTATE mdbuSerial_RxState;
extern mdbuSerial_RxPacket mdbuSerial_RxPkt;

// Registers
FLT_STAT_REG0_Obj Fault_Status_Reg;
VGS_STAT_REG1_Obj VGS_Status_Reg;
DRV_CTRL_REG2_Obj Driver_Control_Reg;
GATE_DRV_HS_REG3_Obj Gate_Drive_HS_Reg;
GATE_DRV_LS_REG4_Obj Gate_Drive_LS_Reg;
OCP_CTRL_REG5_Obj OCP_Control_Reg;
CSA_CTRL_REG6_Obj CSA_Control_Reg;
REG_MAP_Obj Reg_Map_Cache;


/*function
 * DRV8x_State_Machine(void)
 * Handles the state machine and transitions of the Application
 * */
void DRV8x_StateMachine(void)
{
	/* Move to FAULT state if any fault reported by the ISR. ISR is also
	 * changing the state to FAULT. But one extra level of protection to prevent
	 * over writing of the state variable during context switchings */
	if (applicationStatus.fault)
	{
        applicationStatus.previousstate = applicationStatus.currentstate;
        applicationStatus.currentstate = FAULT;
	}

    switch(applicationStatus.currentstate)                    							 	/* process current motor state*/
    {
    case SYSTEM_INIT:
        DRV8x_Digital_Init();                                              				   /*Initialize MSP430F5529 peripherals */
        sensoredTrapController_Init();                                                     /*Initialize Motor Variables*/

        // if(HostController.EnabledGateDrivers)
        // {
        //     drv832x_regRestoreFromCache();												   // Restore device register values from the cached ones
        //     // doesn't do anything for non-spi driver chip
        // }

        applicationStatus.previousstate = applicationStatus.currentstate;                  /* Move to Motor INIT
                                                                                              state and initialize Motor variables*/
        applicationStatus.currentstate = SYSTEM_IDLE;
        break;
    case SYSTEM_IDLE:
        /* If received MOTOR START command, move to MOTOR_INIT */
        if(HostController.StartStopMotor == 0)
        {
            applicationStatus.previousstate = applicationStatus.currentstate;
            applicationStatus.currentstate = MOTOR_IDLE;
	    }
        break;
    case MOTOR_IDLE:
        /* Update direction on LEDs*/
        if(sensoredTrapController.Direction == TRUE)
        {
            P1OUT |= BIT0;                                                              /* Turn  on LED1 */
            P4OUT &= ~BIT7;                                                             /* Turn off LED 2*/
        }
        else
        {
            P1OUT &= ~BIT0;                                                             /* Turn  off LED1 */
            P4OUT |= BIT7;                                                              /* Turn on LED 2*/
        }
        if (sensoredTrapController.TargetDutyCycle > sensoredTrapController.MINDutyCycle)
        {
            /* Move to MOTOR_RAMP_UP if MOTOR START received*/
            if(HostController.StartStopMotor == 0)
            {
            	if(sensoredTrapController.PWM_Mode == 0)  // If six PWM mode
            	{
            		P2OUT &= ~(BIT4 | BIT5);                                    /* Reset bits P2.4 , P2.5  */
            		P2SEL &= ~(BIT4 | BIT5);                                                                    /* Select P2.4 , P2.5 as I/O Function for Phase A*/

            		/* Reset switches for phase B (LOW-HIGH)*/
            		P1OUT &= ~(BIT4 | BIT5);                                    /* Reset bits P1.4 , P1.5  */
            		P1SEL &= ~(BIT4 | BIT5);                                                                    /* Select P1.4 , P1.5 as I/O Function for Phase B*/

            		/* Reset switches for phase C (LOW-HIGH)*/
            		P1OUT &= ~(BIT2 | BIT3);                                    /* Reset bits P1.2 , P1.3  */
            		P1SEL &= ~(BIT2 | BIT3);                                                                    /* Select P1.2 , P1.3 as I/O Function for Phase C*/
            	}
            	else
            	{
                	P1OUT |= BIT2;                  // Motor brakes are removed
            		P2OUT |= (BIT5);
            		P2SEL |= (BIT5);
            	}
            	UpdateNextCommutation();
                TIMER_SPD_Init();																		   // Timer Initialization to read the motor electrical speed

                applicationStatus.previousstate = applicationStatus.currentstate;
                applicationStatus.currentstate = MOTOR_RAMP_UP;
            }
        }
        break;
    case MOTOR_RUN:
        if(sensoredTrapController.Direction == TRUE)
        {
            P1OUT |= BIT0;                                                                   /* Turn  on LED1 */
            P4OUT &= ~BIT7;                                                                  /* Turn off LED 2*/
        }
        else
        {
            P1OUT &= ~BIT0;                                                                  /* Turn  off LED1 */
            P4OUT |= BIT7;                                                                   /* Turn on LED 2*/
        }
        /* Move to MOTOR_RAMP_DOWM if MOTOR STOP received*/
        if(HostController.StartStopMotor == 1)
        {
            applicationStatus.previousstate = applicationStatus.currentstate;
            applicationStatus.currentstate = MOTOR_RAMP_DOWN;
        }

        /*Measure every 100th PWM periods for changed speed input
           and change to acceleration or brake state*/
        if(sensoredTrapController.Accelerationcounter >
           SPEED_INPUT_SAMP_INTERVAL)
        {
            sensoredTrapController.Accelerationcounter = 0;

            if(sensoredTrapController.TargetDutyCycle <
               sensoredTrapController.CurrentDutyCycle)
            {
                applicationStatus.previousstate =
                    applicationStatus.currentstate;
                applicationStatus.currentstate = MOTOR_RAMP_DOWN;
                P1OUT &= ~BIT0;                                                              /* Turn  off LED1 */
                P4OUT &= ~BIT7;                                                              /* Turn off LED 2*/
            }
            else if(sensoredTrapController.TargetDutyCycle >
                    sensoredTrapController.CurrentDutyCycle)
            {
                applicationStatus.previousstate =
                    applicationStatus.currentstate;
                applicationStatus.currentstate = MOTOR_RAMP_UP;
                P1OUT &= ~BIT0;                                                              /* Turn  off LED1 */
                P4OUT &= ~BIT7;                                                              /* Turn off LED 2*/
            }
            else
            {
              if(sensoredTrapController.CurrentDutyCycle < sensoredTrapController.MINDutyCycle)
              {
                    applicationStatus.previousstate =
                        applicationStatus.currentstate;
                    applicationStatus.currentstate = MOTOR_RAMP_DOWN;
                    P1OUT &= ~BIT0;                                                              /* Turn  off LED1 */
                    P4OUT &= ~BIT7;                                                              /* Turn off LED 2*/
              }
            }
            if (sensoredTrapController.TargetDutyCycle > sensoredTrapController.MAXDutyCycle)
            {
                    sensoredTrapController.TargetDutyCycle = sensoredTrapController.MAXDutyCycle;
            }
        }
        break;
    case MOTOR_STOP:
        applicationStatus.previousstate = applicationStatus.currentstate;
        applicationStatus.currentstate = SYSTEM_IDLE;
        break;
    case FAULT:
        DisableGateDrivers();                                                                /* Disable Gate Drivers when a fault is triggered*/
        /*handles the faults and sets fault LEDS*/
        switch(applicationStatus.fault)
        {
        case NOFAULT:           break;
        case HALLSENSORINVALID:
            P1OUT &= ~BIT0;                                                                  /* Turn OFF LED1*/
            P4OUT |= BIT7;                                                                   /* Turn ON LED2*/

            break;
        case MOTOR_STALL:
            P1OUT |= BIT0;                                                                   /* Turn ON LED1*/
            P4OUT ^= BIT7;                                                                   /* Toggle LED2 */

            break;
        case VOLTAGE:

            P1OUT ^= BIT0;                                      							 /* Toggle LED1*/
            P4OUT ^= BIT7;                                      							 /* Toggle LED2 */

            break;
        case OVERCURRENT:
            P1OUT ^= BIT0;                                      							 /* Toggle LED1 */
            P4OUT |= BIT7;                                      							 /* Turn ON LED2 */

            break;
        case OVERTEMPERATURE:
            P1OUT |= BIT0;                                      							 /* Turn ON LED1*/
            P4OUT &= ~BIT7;                                    								 /* Turn OFF LED2*/

            break;
        case GATE_DRIVER:
            P1OUT |= BIT0;                                     								 /* Turn ON LED1*/
            P4OUT |= BIT7;                                     								 /* Turn ON LED2*/

            break;
        case UNKNOWN:                                           							 /* If the fault is Triggered from sources other than described above read the Fault register IC values from Fault Variables */
            P1OUT &= ~BIT0;                                                                  /* Turn OFF LED1*/
            P4OUT &= ~BIT7;                                                                  /* Turn OFF LED2*/

            break;

        default: break;
        }
    }
}

/*function
 * check input pins to see if any changed and
 * do stuff if based on pin states
 *      start
 *      stop
 *      set direction
 * Available pins for input:
 * P7.4
 * P8.1 (unless DRV8323)
 * CHECK: enable may already be set up on P1.6, J1 PIN 9
 * P3.3 (software debug pin)    J1 PIN 7
 * P3.4 (software debug pin)    J1 PIN 5
  * P4.1 (software debug pin)
 * P4.2 (software debug pin)
 * */
void CheckInputPins(void) {
    // // check enable pin P3.3
    // if ((P3IN & BIT3) == 0) {    // P3.3
    if ((P7IN & BIT4) == 0) {   // P7.4
        drv832x_StopMotor();    // stop motor
    } else {
        // sensoredTrapController.TargetDutyCycle = sensoredTrapController.MAXDutyCycle;
        drv832x_StartMotor();    // start motor
    }
}

/*function
 * HostController_StateMachine(void)
 * Handles the HOST state machine
 * */
void HostController_StateMachine(void)
{
	switch(HostControl_Status)
	{
		case HOST_IDLE:
		{
            /*
			uint8_t *data,length;
			uint8_t i,cmd;
			HostControl_Status = HOST_IDLE;
			if(mdbuSerial_rxStateStop)
			{
				cmd =  mdbuSerial_RxPkt.pkt_cmd;
				data =  mdbuSerial_RxPkt.pkt_data;
				length = mdbuSerial_RxPkt.pkt_len;
				mdbuSerial_rxStateStop = 0;
				for(i=0;i<MDBUSERIAL_NUM_CALLBACKS && MDBUSERIAL_NUM_CALLBACKS != 0;i++)
				{
					if(mdbuserial_callbacktable[i].cmd == cmd)
					{
						mdbuserial_callbacktable[i].callback(data, (size_t)(length));
					}
				}
				if(HostController.EnabledGateDrivers)
				{
					HostControl_Status = HOST_ACTIVE;
                    // Default values are read by GUI after enabling the pre drivers
                    drv832x_regRestoreFromCache();  // doesn't do anything for H variant (hardware config instead of SPI)
				}
			}
            */
            if(HostController.EnabledGateDrivers)
            {
                HostControl_Status = HOST_ACTIVE;
            }

		}
		break;

		case HOST_ACTIVE:
		{
            if(HostController.EnabledGateDrivers == 0)
            {
                HostControl_Status = HOST_IDLE;
            }
            /*
            // CheckInputPins();    // disable until enable gate drivers works

			uint8_t *data,length;
			uint8_t i,cmd;
			if(mdbuSerial_rxStateStop)
			{
				cmd =  mdbuSerial_RxPkt.pkt_cmd;
				data =  mdbuSerial_RxPkt.pkt_data;
				length = mdbuSerial_RxPkt.pkt_len;

				mdbuSerial_rxStateStop = 0;
				for(i=0;i<MDBUSERIAL_NUM_CALLBACKS && MDBUSERIAL_NUM_CALLBACKS != 0;i++)
				{
					if(mdbuserial_callbacktable[i].cmd == cmd)
					{
						mdbuserial_callbacktable[i].callback(data, (size_t)(length));
					}
				}
				if(HostController.EnabledGateDrivers == 0)
				{
					HostControl_Status = HOST_IDLE;
				}
			}
            */
		}
		break;
	}
}

void EnableGateDriver(void) {
    // set direction to match pin

    P1OUT |= EN_DRV;    // enable gate driver on port 1.6 , logic high to enable
    HostController.EnabledGateDrivers = 1;
}

void delay_ms(unsigned int ms)
{
    while (ms)
    {
        __delay_cycles(25000);   // 1000 for 1MHz and 16000 for 16MHz
        ms--;
    }
}

/*function
 * void main()
 * Initializes the Application and calls periodically the state machine function
 * */
void main()
{
    Application_Init();                     												 /* Initialize application state machine states*/
    // mdbuSerial_init(); 					// Initialise MDBU Serial Protocol
    HostControllerInit();				// Initialise Host Controller
    EnableGateDriver();                 // set P1.6 high
    // HostController.EnabledGateDrivers = 0x01;
    // HostControl_Status = HOST_ACTIVE;

    while(1)
    {
        DRV8x_StateMachine();                       										 /* call background state machine */
        // HostController_StateMachine();
        CheckInputPins();
    }
}

